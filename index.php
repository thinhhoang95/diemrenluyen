﻿<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Đánh giá điểm rèn luyện</title>
    <link rel="stylesheet" href="styles/reset.css" />
    <link href='//fonts.googleapis.com/css?family=Open+Sans:400,700,300&subset=latin,vietnamese' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="styles/global.css" />
    <script src="js/jquery-2.1.3.min.js"></script>
</head>
<body>
    <?php

    //header("Location: redirect.html");
    //die();

    include_once "DiemRenLuyen.php";
    if(isset($_GET["id"])) $gtoken=$_GET["id"]; else die("Token ID not specified");
    $diemRenLuyen=new DiemRenLuyen($gtoken);
    if(isset($diemRenLuyen->error)){
        die($diemRenLuyen->error);
    }
    ?>
    <style>
        .__Header .__PageWrapDesktop .ActionText{
            font-weight: bold;
        }
        .GuideHeader{
            font-weight: bold;
            padding-top: 20px;
            padding-bottom: 20px;
        }
    </style>
    <div class="__Header">
        <div class="__PageWrapDesktop">
            <div class="ActionText">DIVERGENT PROCEDURES | ĐIỂM RÈN LUYỆN</div>
            <div class="_FillHorizontal" style="flex: 1 0 20px"></div>
            <div class="UserInfo"><?php echo $diemRenLuyen->hoten?> (<?php echo $diemRenLuyen->mssv?>)</div>
        </div>
    </div>
    <div class="__Main">
        <div class="__PageWrapDesktop">
            <p style="font-weight: bold; text-align: center; padding-top: 20px; color: #3F51B5">HƯỚNG DẪN ĐÁNH GIÁ ĐIỂM RÈN LUYỆN THÔNG QUA DIVERGENT PROCEDURES | ĐIỂM RÈN LUYỆN</p>
            <p class="GuideHeader">BƯỚC 1: TẢI TẬP TIN VÀ ĐIỀN</p>
            <ul>
                <li>Bấm vào <a href="danhgia.xlsx">đây</a> để tải các tập tin đánh giá điểm rèn luyện.</li>
                <li>Mở tập tin bằng Excel, các ô có màu vàng là các ô bạn có thể điền thông tin vào. Hãy đọc hướng dẫn ở các cột bên cạnh và thực hiện công việc tương ứng. Điểm sẽ được tự động tính sẵn.</li>
                <li>Bạn cần cố gắng thay đổi các hạng mục để đạt được điểm số tối đa.</li>
                <li>Một số thông tin đã có gợi ý trước để đạt điểm cao.</li>
            </ul>
            <p class="GuideHeader">BƯỚC 2: KIỂM TRA KỸ LƯỠNG LẠI TẬP TIN</p>
            <ul>
                <li>Vui lòng kiểm tra kỹ lại <span style="font-weight: bold; color: #3F51B5">họ tên, mã số sinh viên</span> của bạn.</li>
                <li>Bạn có thể nộp nhiều bản tự đánh giá, bản cuối cùng sẽ là bản được xem xét.</li>
            </ul>
            <p class="GuideHeader">BƯỚC 3: TẢI LÊN TẬP TIN ĐÃ ĐÁNH GIÁ</p>
            <p>Chúng tôi sẽ giúp bạn xác thực tập tin đã tải lên và đảm bảo là quá trình nộp hoàn tất.</p>
            <p>Tình trạng bản nộp: <span style="color: #FFF; padding: 3px; background-color: <?php echo $diemRenLuyen->fillstatus ? '#4CAF50' : '#F44336' ?>; font-weight: bold"><?php echo $diemRenLuyen->fillstatus?"ĐÃ HOÀN THÀNH":"CHƯA HOÀN THÀNH"; ?></span></p>
            <input style="padding-top: 10px" type="file" id="fileupload" name="fileupload" />
            <div><input type="button" id="upload" value="Tải lên" /></div>
            <div id="loading" style="display: none">Xin chờ...</div>
            <p class="GuideHeader">MỘT SỐ LƯU Ý CỦA QUẢN TRỊ VIÊN</p>
            <p>Đây không phải là kết quả cuối cùng. BCS sẽ gửi cho bạn kết quả chính thức qua email sinh viên, dự kiến vào ngày 10/9/2016. Mọi thắc mắc hay khiếu nại vui lòng liên hệ với Tường Anh hoặc Đình Thịnh.</p>
        </div>
    </div>
<script>
    ///Handle Upload Button Click event
    $("#upload").on("click", function () {
        $(this).hide();
        var file_data = $("#fileupload").prop("files")[0];
        var form_data = new FormData;
        form_data.append("file", file_data);
        form_data.append("id", "<?php echo $gtoken ?>");
        $.ajax({
            url: "upload.php",
            dataType: 'text',
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            success: function (response) {
                alert("Phản hồi: " + response);
                window.location = window.location;
            }
        });
    });
</script>

</body>
</html>
