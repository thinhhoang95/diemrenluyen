<?php

/**
 * DiemRenLuyen short summary.
 *
 * DiemRenLuyen description.
 *
 * @version 1.0
 * @author Thinh
 */
include_once "PHPExcel.php";

$DbConnection=new mysqli("localhost","root","admin","diemrenluyen") or die("Error connecting to database!");
$DbConnection->query("SET NAMES utf8");

class DiemRenLuyen
{
    public $mssv;
    public $hoten;
    public $token;
    public $tokenid;
    public $error;
    public $fillstatus;
    ///Construct will fill mssv, hoten from given token.
    public function __construct($gtoken){
        $this->token=$gtoken;
        global $DbConnection;
        $result=$DbConnection->query("SELECT tokens_id, tokens_mssv, tokens_hoten FROM tokens WHERE tokens_token='".$gtoken."'");
        if($result->num_rows>0){
            $row=$result->fetch_array();
            $this->tokenid=$row["tokens_id"];
            $this->hoten=$row["tokens_hoten"];
            $this->mssv=$row["tokens_mssv"];
            $result=$DbConnection->query("SELECT log_id FROM log WHERE log_tokens_id=".$this->tokenid);
            if($result->num_rows>0) $this->fillstatus=true; else $this->fillstatus=false;
        } else {
            $this->error="Invalid token ID.";
        }
    }
    private function RegisterLog(){
        global $DbConnection;
        $DbConnection->query("INSERT INTO log (log_tokens_id) VALUES ('".$this->tokenid."')");
    }
    private function WriteDRL($mssv,$hoten,$drl)
    {
        global $DbConnection;
        $DbConnection->query("INSERT INTO diem (mssv,hoten,diemrenluyen) VALUES ('$mssv','$hoten','$drl')");
    }
    public function ValidateFile(){
        global $_FILES;
        $ext=pathinfo($_FILES["file"]["name"])['extension'];
        //Move the file to TEMP dir
        move_uploaded_file($_FILES['file']['tmp_name'], 'temp/'.$this->token.".".$ext);
        //Now check the file
        $workFile='temp/'.$this->token.".".$ext;
        $objPHPExcel=PHPExcel_IOFactory::load($workFile);
        $mssvFromFile=$objPHPExcel->getActiveSheet()->getCell('C7')->getValue();
        $hotenFromFile=$objPHPExcel->getActiveSheet()->getCell('C6')->getValue();
        $drlFromFile=$objPHPExcel->getActiveSheet()->getCell('F52')->getOldCalculatedValue();
        $objPHPExcel->disconnectWorksheets();
        unset($objPHPExcel);
        if(strtolower($mssvFromFile)==strtolower($this->mssv)){
            //If OK, move the file to uploads dir and register an entry in the log book
            rename($workFile,'uploads/'.$this->mssv.".".$ext);
            $workFile='uploads/'.$this->mssv.".".$ext;
            $this->RegisterLog();
            $this->WriteDRL($mssvFromFile, $hotenFromFile, $drlFromFile);
            echo "File was uploaded successfully. Thank you.";
        } else {
            unlink($workFile);
            echo "File validity check failed. Please check the field \"MSSV\" in the Excel file and try again.";
        }
    }
}
